"use strict";

var counter = 0;

var button = document.getElementById('send-event-button');

button.addEventListener('click', () => {
    counter++;

    console.log('sending click event...');
    Telemetry.sendEvent('button', {
        clicked: counter
    });
});


var Telemetry = (function() {
    const APP_REGION = "eu-west-1";
    const APP_VERSION_NAME = "0.0.1";
    const APP_VERSION_CODE = '1';
    const APP_PACKAGE_NAME = 'golf.futuresecretgames.com';

    AWS.config.region = APP_REGION;
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: "eu-west-1:ba398ed9-aad7-402d-a0c0-b666455e1832"
    });

    var analyticsOptions = {
        appId: "e7963fc3d292429683fdd1edf9eebb5a",
        appTitle: "PaperGolf_Dev",
        appVersionName: APP_VERSION_NAME,
        appVersionCode: APP_VERSION_CODE,
        appPackageName: APP_PACKAGE_NAME
    };

    var analyticsClient = new AMA.Manager(analyticsOptions);

    /**
     * Provides functions for sending telemetry information to the analytics server.
     */
    return {

        /**
         * Sends a telemetry event to the server.
         * @param {String} eventName - Name associated with this telemetry event.
         * @param {object=null} eventData - Data associated with the telemetry event.
         */
        sendEvent: function (eventName, eventData) {
            if (!eventName) {
                throw new Error('Telemetry.sendEvent - No event name was specified.');
            }

            if (analyticsClient) {
                analyticsClient.recordEvent(eventName, eventData, {testing: 1});
            }
        },

        /**
         * Forces any cached telemetry data to be sent to the server.
         */
        forceSend: function () {
            if (analyticsClient) {
                analyticsClient.submitEvents();
            }
        }
    };
}());
