Telemetry Test
==============
Project for testing integration with AWS analytics.

Installation
============
Clone repository
Navigate to folder and run:
```
npm install
```
from the command line.

Once installed, you may run the local server with:
```
npm run dev
```
Open a web-browser and navigate to 127.0.0.1:8081
