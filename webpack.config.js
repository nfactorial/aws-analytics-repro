var path = require('path');

var module_dir = __dirname + '/node_modules';

var config = {
    addVendor: function(name, path) {
        this.resolve.alias[name] = path;
        //this.module.noParse.push(new RegExp(path));
    },
    context: __dirname + '/development',
    resolve: {
        alias: {}
    },
    entry: {
        app: ['./main.js']
    },
    devServer: {
        contentBase: './development',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        noParse: [],
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                include: [
                    path.resolve(__dirname, 'development')
                ],
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};

module.exports = config;